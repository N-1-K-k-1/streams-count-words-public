package com.efimchick.ifmo.streams.countwords;


import java.util.*;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Words {

    public String countWords(List<String> lines) {

        Pattern pattern = Pattern.compile("[а-яa-z]{4,}");

        List<String> lineWords = new ArrayList<>();

        lines.forEach(line -> {
            lineWords.addAll(
                    Stream.of(line)
                            .map(word -> word.toLowerCase())
                            .map(word -> word.split("[\\s[^а-яa-z]+]+"))
                            .flatMap(words -> Arrays.stream(words))
                            .filter(word -> word.length() >= 4)
                            .collect(Collectors.toList())
            );
        });

        Map<String, Integer> countedWords = lineWords
                .stream()
                .collect(Collectors.groupingBy(
                        Function.identity(),
                        Collectors.summingInt(value -> 1)
                        ));


        LinkedHashMap<String, Integer> sortedCountedWords = countedWords
                .entrySet()
                .stream()
                .filter(entry -> entry.getValue() >= 10)
                .sorted(Map.Entry.<String, Integer>comparingByValue(Comparator.reverseOrder())
                .thenComparing(Map.Entry.comparingByKey()))
                .collect(Collectors.toMap(
                        entry -> entry.getKey(),
                        entry -> entry.getValue(),
                        (v1, v2) -> {
                            throw new IllegalStateException();
                        },
                        () -> new LinkedHashMap<>()
                ));

        String stringOfWords = sortedCountedWords
                .entrySet()
                .stream()
                .map(entry -> entry.getKey() + " - " + entry.getValue())
                .collect(Collectors.joining("\n"));



        return stringOfWords;
    }
}
